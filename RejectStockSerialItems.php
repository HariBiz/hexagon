<?php
/* $Id: StockSerialItems.php 6942 2014-10-27 02:48:29Z daintree $*/

include('includes/session.inc');
$Title = _('Stock Of Controlled Items');
include('includes/header.inc');
echo '<p class="page_title_text">
		<img src="'.$RootPath.'/css/'.$Theme.'/images/inventory.png" title="' . _('Inventory') .
'" alt="" /><b>' . $Title. '</b>
	</p>';
$qury="";
$qury2="";

if (isset($_GET['PONumber']))
{
	$orderno=$_GET['PONumber'];
	$qury= " AND orderno = '" . $orderno . "'";
}
if (isset($_GET['Location']))
{
	$qury2=" AND stockserialitems.loccode='" . $_GET['Location'] . "'";
}

for ($x = 0; $x <= $_POST['Count']; $x++) 
{
	
	if(isset($_POST['Reject'.$x]))
	{
		//echo "reject line ".$x;
		$rejected_qty=$_POST['quantity'.$x];
		$SupplierID="SELECT `supplierno` FROM `purchorders` WHERE `orderno`='".$_POST['OrderNo']."'";
		$result_SupplierID = DB_query($SupplierID);
		$myrow_result_SupplierID = DB_fetch_array($result_SupplierID);
		$result_SupplierID1=$myrow_result_SupplierID['supplierno'];
		$sql = "INSERT INTO rejected_po (stockid,
												supplierid,
												orderno,
												rejected_qty,
												rejected_by,
												rejected_date,
												Batch_no)
							VALUES ('".$_POST['StockId']."',
								'" . $result_SupplierID1 . "',
								'" . $_POST['OrderNo'] . "',
								'" .$rejected_qty. "',
								'" .$_SESSION['UserID']. "',
								'" .date("Y-m-d"). "',
								'" . $_POST['serialno'.$x] . "')";
					$ErrMsg =  _('The rejected item could not be added because');
					$DbgMsg = _('The SQL that was used to add the item failed was');
					$result = DB_query($sql, $ErrMsg, $DbgMsg,'',true);
					
		$lockStockqty="SELECT `quantity` FROM `locstock` WHERE `stockid`='".$_POST['StockId']."'";
		$result_lockStockqty = DB_query($lockStockqty);
		$myrow_lockStockqty = DB_fetch_array($result_lockStockqty);
		$result_lockStockquantity=$myrow_lockStockqty['quantity'];
		
		$current_quantity=$result_lockStockquantity-$rejected_qty;
		
		$lockStockqty="UPDATE `locstock` SET `quantity`= '".$current_quantity."' WHERE `stockid`='".$_POST['StockId']."'";
		$result_lockStockqty = DB_query($lockStockqty);
		
		$getemailid="select email from www_users where defaultlocation='".$_SESSION['DefaultFactoryLocation']."' AND fullaccess=19 AND EmailNotification=1";
		$result_getemailid = DB_query($getemailid);
		
		while ($myrow_getemailid=DB_fetch_array($result_getemailid)) 
		{
			
			
			//$myrow_getemailid = DB_fetch_array($result_getemailid);
			//$emailid=$myrow_getemailid['email'];
			$to = $myrow_getemailid['email'];
			$subject = "Hexegon test mail";
			$txt = "the user '" .$_SESSION['UsersRealName']. "' has rejected '" .$rejected_qty. "' quantity of the item '".$_POST['StockId']."'";
			$headers = "From:admin@bizessence.in" . "\r\n" ."CC: hari@bizessence.in";
			mail($to,$subject,$txt,$headers);
			
		}
		$rejettableDELETE="DELETE FROM `stockserialitems` WHERE `stockid`='".$_POST['StockId']."' AND serialno='".$_POST['serialno'.$x]."'";
		$result_DELETE = DB_query($rejettableDELETE);
		if($result_DELETE==1)
		{
			
			echo '<br />
		<div class="centre">
			'. prnMsg(_('Batch number'). ' '. $_POST['serialno'.$x] .' '. _('has been Rejected'),'success') . '
			<br />
			<br />
		</div>';
			
		}
		else
		{
			prnMsg(_('Batch is not rejected contact administrator'),'error');
		}
	
		
	
	}
	

}
if(isset($_POST['RejectNormal']))
{ 

		$rejected_qty=$_POST['quantityNormal'];
		$SupplierID="SELECT `supplierno` FROM `purchorders` WHERE `orderno`='".$_POST['OrderNo']."'";
		$result_SupplierID = DB_query($SupplierID);
		$myrow_result_SupplierID = DB_fetch_array($result_SupplierID);
		$result_SupplierID1=$myrow_result_SupplierID['supplierno'];
		$sql = "INSERT INTO rejected_po (stockid,
												supplierid,
												orderno,
												rejected_qty,
												rejected_by,
												rejected_date)
							VALUES ('".$_POST['StockId']."',
								'" . $result_SupplierID1 . "',
								'" . $_POST['OrderNo'] . "',
								'" .$rejected_qty. "',
								'" .$_SESSION['UserID']. "',
								'" .date("Y-m-d"). "')";
					$ErrMsg =  _('The rejected item could not be added because');
					$DbgMsg = _('The SQL that was used to add the item failed was');
					$result = DB_query($sql, $ErrMsg, $DbgMsg,'',true);
					
		$lockStockqty="SELECT `quantity` FROM `locstock` WHERE `stockid`='".$_POST['StockId']."'";
		$result_lockStockqty = DB_query($lockStockqty);
		$myrow_lockStockqty = DB_fetch_array($result_lockStockqty);
		$result_lockStockquantity=$myrow_lockStockqty['quantity'];
		
		$current_quantity=$result_lockStockquantity-$rejected_qty;
		
		$lockStockqty="UPDATE `locstock` SET `quantity`= '".$current_quantity."' WHERE `stockid`='".$_POST['StockId']."'";
		$result_lockStockqty = DB_query($lockStockqty);
		
		//select email from www_users inner join locationusers on locationusers.userid=www_users.userid where www_users.
		
		$getemailid="select email from www_users where defaultlocation='".$_SESSION['DefaultFactoryLocation']."' AND fullaccess=19 AND EmailNotification=1";
		$result_getemailid = DB_query($getemailid);
		
		while ($myrow_getemailid=DB_fetch_array($result_getemailid)) 
		{
			
			//$myrow_getemailid = DB_fetch_array($result_getemailid);
			//$emailid=$myrow_getemailid['email'];
			$to = $myrow_getemailid['email'];
			$subject = "Hexegon test mail";
			$txt = "the user '" .$_SESSION['UsersRealName']. "' has rejected '" .$rejected_qty. "' quantity of the item '".$_POST['StockId']."'";
			$headers = "From:admin@bizessence.in" . "\r\n" ."CC: hari@bizessence.in";
			mail($to,$subject,$txt,$headers);
			prnMsg(_('Email notification is sent to ').$to. '  '. _(''),'success');
			
			$table2 .= '
		<tr>
			<td>'.$notification.'</td>
			<td>'.$date.'</td>
		</tr>';	
		}
		if($result_lockStockqty==1)
		{
			echo '<br />
		<div class="centre">
			'. prnMsg(_('item'). '  '. _('has been Rejected'),'success') . '
			<br />
			<br />
		</div>';
			
		}
		else
		{
			prnMsg(_('Batch is not rejected contact administrator'),'error');
		}


}
if (isset($_GET['StockID'])){
	if (ContainsIllegalCharacters ($_GET['StockID'])){
		prnMsg(_('The stock code sent to this page appears to be invalid'),'error');
		include('includes/footer.inc');
		exit;
	}
	$StockID = trim(mb_strtoupper($_GET['StockID']));
} 
elseif(isset($_POST['OrderNo']))
{
	
}
else 
{
	prnMsg( _('This page must be called with parameters specifying the item to show the serial references and quantities') . '. ' . _('It cannot be displayed without the proper parameters being passed'),'error');
	include('includes/footer.inc');
	exit;
}
$result = DB_query("SELECT description,
							units,
							mbflag,
							decimalplaces,
							serialised,
							controlled,
							perishable
						FROM stockmaster
						WHERE stockid='".$StockID."'",
						_('Could not retrieve the requested item because'));

$myrow = DB_fetch_array($result);
$Description = $myrow['description'];
$UOM = $myrow['units'];
$DecimalPlaces = $myrow['decimalplaces'];
$Serialised = $myrow['serialised'];
$Controlled = $myrow['controlled'];
$Perishable = $myrow['perishable'];

if ($myrow['mbflag']=='K' OR $myrow['mbflag']=='A' OR $myrow['mbflag']=='D'){

	prnMsg(_('This item is either a kitset or assembly or a dummy part and cannot have a stock holding') . '. ' . _('This page cannot be displayed') . '. ' . _('Only serialised or controlled items can be displayed in this page'),'error');
	include('includes/footer.inc');
	exit;
}

$result = DB_query("SELECT locationname
						FROM locations
						INNER JOIN locationusers ON locationusers.loccode=locations.loccode AND locationusers.userid='" .  $_SESSION['UserID'] . "' AND locationusers.canview=1
						WHERE locations.loccode='" . $_GET['Location'] . "'",
						_('Could not retrieve the stock location of the item because'),
						_('The SQL used to lookup the location was'));

$myrow = DB_fetch_row($result);

 $sql = "SELECT serialno,
				quantity,
				expirationdate
			FROM stockserialitems
			INNER JOIN locationusers ON locationusers.loccode=stockserialitems.loccode AND locationusers.userid='" .  $_SESSION['UserID'] . "' AND locationusers.canview=1
			WHERE 1 
			AND stockid = '" . $StockID . "'
			".$qury."
			".$qury."
			AND quantity <>0";


$ErrMsg = _('The serial numbers/batches held cannot be retrieved because');
$LocStockResult = DB_query($sql, $ErrMsg);
echo '<form action="' . htmlspecialchars($_SERVER['PHP_SELF'].'?PONumber='.$_GET['PONumber'].'&StockID='.$_GET['StockID'],ENT_QUOTES,'UTF-8') . '" method="post">';
echo '<input type="hidden" name="FormID" value="' . $_SESSION['FormID'] . '" />';
if($Serialised==0 AND $Controlled==0)
{
	echo '<table class="selection">';
	echo '<tr><th colspan="3"><font color="navy" size="2">' . _('Non controlled item') . ' ';
	echo '</font></th></tr>';
	echo '<tr><td>' . _('Quantity to be rejected') . '' . _(':') . '</td>
	<td><input type="text" name="quantityNormal" value="" size="10"/></td>
	<td><input type="submit" name="RejectNormal" value=\'Reject\' /><br /></td></tr>';
	echo '</table>';
	echo'<input type="hidden" name="StockId" value="'.$_GET['StockID'].'" />';
echo'<input type="hidden" name="OrderNo" value="'.$_GET['PONumber'].'" />';
}
else
{
echo '<table class="selection">';

if ($Serialised==1){
	echo '<tr><th colspan="8"><font color="navy" size="2">' . _('Serialised items in') . ' ';
} else {
	echo '<tr><th colspan="14"><font color="navy" size="2">' . _('PO Number  - '). $_GET['PONumber'] . ' ';
}
echo $myrow[0]. '</font></th></tr>';

echo '<tr>
		<th colspan="14"><font color="navy" size="2">' . $StockID .'-'. $Description  . '</b>  (' . _('In units of') . ' ' . $UOM . ')</font></th>
	</tr>';
//print_r($_SESSION['UsersRealName']);
if ($Serialised == 1 and $Perishable==0){
	$tableheader = '<tr>
						<th>' . _('Serial Number') . '</th>
						<th>' . _('Reject') . '</th>
						<th></th>
						<th>' . _('Serial Number') . '</th>
						<th>' . _('Reject') . '</th>
						<th></th>
						<th>' . _('Serial Number') . '</th>
						<th>' . _('Reject') . '</th>
						<th></th>
					</tr>';
} else if ($Serialised == 1 and $Perishable==1){
	$tableheader = '<tr>
			<th>' . _('Serial Number') . '</th>
			<th>' . _('Expiry Date') . '</th>
			<th>' . _('Serial Number') . '</th>
			<th>' . _('Expiry Date') . '</th>
			<th>' . _('Serial Number') . '</th>
			<th>' . _('Expiry Date') . '</th>
			<th>' . _('Reject') . '</th>
			</tr>';
} else if ($Serialised == 0 and $Perishable==0){
	$tableheader = '<tr>
						<th>' . _('Batch/Bundle Ref') . '</th>
						<th>' . _('Quantity') . '</th>
						<th>' . _('Reject') . '</th>
						<th></th>
						<th>' . _('Batch/Bundle Ref') . '</th>
						<th>' . _('Quantity') . '</th>
						<th>' . _('Reject') . '</th>
						<th></th>
						<th>' . _('Batch/Bundle Ref') . '</th>
						<th>' . _('Quantity') . '</th>
						<th>' . _('Reject') . '</th>
					</tr>';
} else if ($Serialised == 0 and $Perishable==1){
	$tableheader = '<tr>
						<th>' . _('Batch/Bundle Ref') . '</th>
						<th>' . _('Quantity') . '</th>
						<th>' . _('Expiry Date') . '</th>
						<th>' . _('Reject') . '</th>
						<th></th>
						<th>' . _('Batch/Bundle Ref') . '</th>
						<th>' . _('Quantity') . '</th>
						<th>' . _('Expiry Date') . '</th>
						<th>' . _('Reject') . '</th>
						<th></th>
			   			<th>' . _('Batch/Bundle Ref') . '</th>
						<th>' . _('Quantity') . '</th>
						<th>' . _('Expiry Date') . '</th>
						<th>' . _('Reject') . '</th>
			   		</tr>';
}
echo $tableheader;
$TotalQuantity =0;
$j = 1;
$Col =0;
$BGColor ='#CCCCCC';
$i=0;
while ($myrow=DB_fetch_array($LocStockResult)) {

	if ($Col==0 AND $BGColor=='#EEEEEE'){
		$BGColor ='#CCCCCC';
		echo '<tr class="EvenTableRows">';
	} elseif ($Col==0){
		$BGColor ='#EEEEEE';
		echo '<tr class="OddTableRows">';
	}

	$TotalQuantity += $myrow['quantity'];

	if ($Serialised == 1 and $Perishable==0){
		echo '<td>' . $myrow['serialno'] . '
			<input type="hidden" name="serialno'.$i.'" value="'.$myrow['serialno'].'" />
		</td>';
		//echo '<input type="hidden" name="DateType" value="'.$_POST['DateType'].'" />';
		echo '<th><input type="submit" name="Reject'.$i.'" value=\'Reject\' /><br /></th>';
		echo '<th></th>';
		
		
	} else if ($Serialised == 1 and $Perishable==1) {
		echo '<td>' . $myrow['serialno'] . '
		<input type="hidden" name="serialno'.$i.'" value="'.$myrow['serialno'].'" />
		</td>
				<td>' . ConvertSQLDate($myrow['expirationdate']). '
				<input type="hidden" name="expirationdate'.$i.'" value="'.$myrow['expirationdate'].'" />
				</td>';
				echo '<th></th>';
	} else if ($Serialised == 0 and $Perishable==0) {
		echo '<td>' . $myrow['serialno'] . '
		<input type="hidden" name="serialno'.$i.'" value="'.$myrow['serialno'].'" />
		</td>
			<td class="number">' . locale_number_format($myrow['quantity'],$DecimalPlaces) . '
			<input type="hidden" name="expirationdate'.$i.'" value="'.$myrow['expirationdate'].'" />
			</td>';
			
		echo '<th></th>';
		echo '<th></th>';
	} else if ($Serialised == 0 and $Perishable==1){
		echo '<td>' . $myrow['serialno'] . '
		<input type="hidden" name="serialno'.$i.'" value="'.$myrow['serialno'].'" /></td>
			<td class="number">' . locale_number_format($myrow['quantity'],$DecimalPlaces). '
			<input type="hidden" name="quantity'.$i.'" value="'.$myrow['quantity'].'" />
			</td>
			<td>' . ConvertSQLDate($myrow['expirationdate']). '
			<input type="hidden" name="expirationdate'.$i.'" value="'.$myrow['expirationdate'].'" /></td>
			<td><input type="submit" name="Reject'.$i.'" value=\'Reject\' /><br /></td>';
			echo '<th></th>';
	}
	$j++;
	$i++;
	If ($j == 36){
		$j=1;
		echo $tableheader;
	}
//end of page full new headings if
	$Col++;
	if ($Col==3){
		echo '</tr>';
		$Col=0;
	}
}
echo'<input type="hidden" name="Count" value="'.$i.'" />';
echo'<input type="hidden" name="StockId" value="'.$_GET['StockID'].'" />';
echo'<input type="hidden" name="OrderNo" value="'.$_GET['PONumber'].'" />';
//end of while loop

echo '</table><br />';
}
if($Serialised==0 AND $Controlled==0)
{
}
else
{
	echo '<div class="centre"><br /><b>' . _('Total quantity') . ': ' . locale_number_format($TotalQuantity, $DecimalPlaces) . '<br /></div>';
}

echo '</form>';
include('includes/footer.inc');
?>
